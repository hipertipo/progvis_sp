size(200, 330)

n = 19
m = 10
x, y = 30, 30
s = 13

for j in range(m):
    # desenha coluna
    for i in range(n):
        fill(random(), random(), random())
        rect(x, y, s, s)
        translate(0, s)
    # termina de desenhar coluna

    # move ponto zero pra direita e pra baixo
    translate(s, -(s*n))
