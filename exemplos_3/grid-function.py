size(200, 330)

def draw_col(n, g):
    c = 1.0 / (n-1)
    for i in range(n):
        r = i * c
        fill(r, g, 0.5)
        rect(x, y, s, s)
        translate(0, s)

n = 11
m = 7
x, y = 24, 18
s = 20

colorstep_g = 1.0 / (m-1)

for j in range(m):
    g = j * colorstep_g
    draw_col(n, g)
    translate(s, -(s*n))
