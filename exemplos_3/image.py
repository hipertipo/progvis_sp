# help(image)

# a local image
img_1 = u"/_projects/progvis-sp/_imgs/programming/Bnjt2QTIcAA7zoe.jpg"

# an image from the web
img_2 = 'http://patcomputerizedworld.files.wordpress.com/2014/03/guido-van-rossum_python.jpg'

x, y = 0, 28
for i in range(5):
    image(img_2, (x, y), .5)
    rotate(-3)
    x += 50
    y += 50
