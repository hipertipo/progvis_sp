size(200, 200)

B = BezierPath()
B.moveTo((24, 168))
B.lineTo((46, 26))
B.lineTo((134, 30))
B.lineTo((162, 170))

strokeWidth(5)
fill(None)
stroke(1, 0, 0)
drawPath(B)

stroke(0, 1, 0)
x, y = 0, 0

# fazer um grid de elementos
# desenhar apenas dentro do bezier

_x = x
for i in range(10):
    for j in range(11):
        if B.pointInside((_x, y)):
            s = randint(5, 15)
            oval(_x, y, s, s)
        _x += 20
    _x = x
    y += 20
