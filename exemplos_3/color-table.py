size(200, 200)

n = 6
m = 10
x, y = 24, 18
s = 15

colorstep_r = 1.0 / (n-1)
colorstep_g = 1.0 / (m-1)

for j in range(m):
    for i in range(n):
        # print j, i
        g = j * colorstep_g
        r = i * colorstep_r
        fill(r, g, 0.5)
        rect(x, y, s, s)
        translate(0, s)
    translate(s, -(s*n))
