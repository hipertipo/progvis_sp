
hyphenation(True)
fontSize(40)
textBox(u'''Já houve um dia em que designers perguntavam a si mesmos: ‘Para que usar um computador para projetar?’. Alguns argumentavam que o computador não era necessário para ser um bom designer. Eles estavam certos.

Alguns anos mais tarde, e quase não há designer trabalhando sem um computador. O computador tornou-se a principal ferramenta do designer (hoje em dia, muitas vezes a única).''', (122, 142, 530, 738))
