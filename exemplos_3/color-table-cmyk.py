size(240, 310)

n, m = 15, 10
x, y = 24, 18
s = 13

colorstep_c = 1.0 / (n-1)
colorstep_m = 1.0 / (m-1)

for j in range(m):
    for i in range(n):
        # print j, i
        M = j * colorstep_m
        C = i * colorstep_c
        cmykFill(C, M, 0.5, 0)
        rect(x, y, s, s)
        translate(0, s)
    translate(s, -(s*n))    
