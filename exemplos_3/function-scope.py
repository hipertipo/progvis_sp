# exemplo: escopo da função

# variável global
a = 10

def my_func():
    # função tem acesso a variáveis globais
    print a
    # variável definida dentro da função
    c = 20

my_func()

# erro: contexto global não tem acesso ao escopo da função
# print c
