# uma função que faz algo
# mas não retorna nenhum valor

def hello(name):
    print 'hello %s' % name

names = ['Maria', 'João', 'José']

for N in names:
    hello(N)

print hello('Jaspion')