# exemplo: dicionário

D = {
    'Andres' : [ 'Sao Paulo', 21, True ],
    'Pedro' : [ 'Rio de Janeiro', 45, False ],
    'Josefina' : [ 'Manaus', 14, False ],
    'Maxwell' :  [ 'Sao Paulo', 36, True ],
    'Zyldian' : [ 'Belo Horizonte', 23, True ],
}

# 1. quais pessoas nasceram em SP?

nomes = []
for nome in D.keys():
    if D[nome][0] == 'Sao Paulo':
        nomes.append(nome)        

print ' e '.join(nomes), 'nasceram em São Paulo.'

# 2. quantas pessoas estão presentes?

presentes = 0
for nome in D.keys():
    if D[nome][2] == True:
        presentes += 1

print presentes, 'pessoas estão presentes.'

# 3. quem tem mais de 22 anos? quantas pessoas são?

nomes = []
for nome in D.keys():
    valores = D[nome]
    idade = valores[1]
    adulto = idade > 22
    if adulto == True:
        nomes.append(nome)
        
print ' e '.join(nomes), 'tem mais de 22 anos (%s pessoas no total).' % len(nomes)
