size(200, 330)

n = 11
m = 7
x, y = 28, 40
s = 20

# criar cópia de trabalho da posição vertical
# para poder volta pra posição original depois
_y = y
for j in range(m):
    # desenha uma coluna
    for i in range(n):
        fill(random(), random(), random())
        rect(x, _y, s, s)
        _y += s
    # termina de desenhar coluna

    x += s # move x pra direita
    _y = y # retorna ao valor inicial (y)
