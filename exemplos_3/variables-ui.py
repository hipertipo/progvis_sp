# Variable --> vanilla power in DrawBot!

from AppKit import NSColor

var_1 = {
    'name' : "aList",
    'ui' : "PopUpButton",
    'args' : {
        'items' : ['a', 'b', 'c', 'd'],
    }
}

var_2 = {
    'name' : "aText",
    'ui' : "EditText",
    'args' : {
        'text' : 'hello world',
    }
}        

var_3 = {
    'name' : "aSlider",
    'ui' : "Slider",
    'args' : {
        'value' : 100,
        'minValue' : 50,
        'maxValue' : 300,
    }
}

var_4 = {
    'name' : "aCheckBox",
    'ui' : "CheckBox",
    'args' : {
        'value' : True
    }
}

c = NSColor.colorWithCalibratedRed_green_blue_alpha_(0, .5, 1, .8)

var_5 = {
    'name' : "aColorWell",
    'ui' : "ColorWell",
    'args' : {
        'color' : c,
    }
}

var_6 = {
    'name' : "aRadioGroup",
    'ui': "RadioGroup",
    'args' : {
        'titles' : ['I', 'II', 'III'],
        'isVertical' : False,
    }
}

Variable([var_1, var_2, var_3, var_4, var_5, var_6,], globals())

print aList
print aText
print aSlider
print aCheckBox
print aColorWell
print aRadioGroup
