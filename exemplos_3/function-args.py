# função com argumentos

def full_name(first_name, last_name):
    # return first_name + ' ' + last_name
    return '%s %s' % (first_name, last_name)

nome_completo = full_name('José', 'da Silva')
print nome_completo
