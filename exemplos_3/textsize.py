size(268, 170)

x, y = 26, 44

fontSize(35)
w, h = textSize('hello world')

# desenha o fundo
fill(1, 1, 0)
rect(x, y, w, h)

# desenha o texto
fill(0)
text('hello world', (x, y))
