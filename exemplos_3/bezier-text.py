size(814, 674)

# fontSize(104)
# text('texto qualquer', (86, 532))

B = BezierPath()
B.text('hi', font='Verdana Bold', fontSize=520, offset=(90, 122))

fill(0, 1, 0)
drawPath(B)

fill(0, 0, 1)
r = 9
for p in B.onCurvePoints:
    x, y = p
    oval(x-r, y-r, r*2, r*2)
