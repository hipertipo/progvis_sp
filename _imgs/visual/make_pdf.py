# make pdf

images = [
    # Joseph Albers
    'albers-circles_00.png',
    'albers-square_00.png',
    'albers-square_01.png',
    'albers-square_02.png',
    'albers-square_03.png',
    # Anni Albers
    'anni-albers_00.png',
    'anni-albers_01.png',
    # Barsotti
    'barsotti_00.png',
    'barsotti_01.png',
    'barsotti_02.png',
    'barsotti_03.png',
    'barsotti_04.png',
    # Max Bill
    'max-bill_01.png',
    'max-bill-1967.png',
    'max-bill-variationen_01.png',
    'max-bill-variationen_00.png',
    # Luis Sacilotto
    'sacilotto_02.png',
    'sacilotto_04.png',
    'sacilotto_00.png',
    'sacilotto_01.png',
    'sacilotto_03.png',
    # Wyllis
    'wyllis_02.png',
    # Richard Paul Lohse
    'richard-paul-lohse_00.png',
    'richard-paul-lohse_01.png',
    'richard-paul-lohse_02.png',
    'richard-paul-lohse_04.png',
    # Franke
    'franke-oscillogramm_00.png',
    'franke-oscillogramm_01.png',
    'franke-oscillogramm_02.png',
    # Whitney
    'whitney_00.png',
    'whitney-kaleidoscope_00.png',
    'whitney-kaleidoscope_01.png',
    'whitney-kaleidoscope_03.png',
    'whitney-kaleidoscope_02.png',
    # spirograph
    'spirograph.png',
    # Ken Knowlton
    'knowlton-nude_00.png',
    'knowlton-nude_01.png',
    'knowlton_00.png',
    'knowlton_01.png',
    # Karl Gerstner
    'gerstner_designing-programs.png',
    'gerstner_01.png',
    # desenho de marcas
    'volkswagen_00.png',
    'stankowski-db.png',
    'aloisio-magalhaes_marcas.png',
    # Otl Aicher: Munique 1972
    'otl-aicher_02.png',
    'otl-aicher_01.png',
    'otl-aicher_00.png',
    # Jurriaan Shrofer
    'jurriaan-shrofer_00.png',
    'jurriaan-shrofer_02.png',
    'jurriaan-shrofer_04.png',
    'jurriaan-shrofer_01.png',
    'jurriaan-shrofer_05.png',
    # Michael Noll
    'michael-noll_01.png',
    'michael-noll_02.png',
    'michael-noll_computer-coreography.png',
    'mondrian_00.png',
    'michael-noll_00.png',
    # Nees, Nake
    'nees_00.png',
    'nake-polygonzug_00.png',
    'nake-polygonzug_01.png',
    # Manfred Mohr
    'manfred-mohr_00.png',
    'manfred-mohr_01.png',
    'manfred-mohr_02.png',
    'manfred-mohr_03.png',
    # Sol Lewitt
    'sol-lewitt_02.png',
    'sol-lewitt_04.png',
    'sol-lewitt_05.png',
    'sol-lewitt_06.png',
    'sol-lewitt_00.png',
    # information visualization
    'tufte-envisioning-information.png',
    'fat-fonts_00.png',
    'fat-fonts_01.png',
]

for img in images:
    newPage(800, 600)
    image(img, (0, 0))

saveImage('../programacao-visual.pdf')
