# make pdf

images = [
    # antigas máquinas de computar
    u"/_projects/progvis-sp/_imgs/computers/abacus_00.png",
    u"/_projects/progvis-sp/_imgs/computers/jaron-lanier.png", 
    u"/_projects/progvis-sp/_imgs/computers/antikythera_00.png",
    u"/_projects/progvis-sp/_imgs/computers/antikythera_01.png",
    # máquina diferencial de Charles Babbage
    u"/_projects/progvis-sp/_imgs/computers/babbage-difference-engine.png",
    u"/_projects/progvis-sp/_imgs/computers/ada-lovelace.png",
    u"/_projects/progvis-sp/_imgs/computers/sketchpad_01.png",
    # Alan Turing, 2a Guerra Mundial
    u"/_projects/progvis-sp/_imgs/computers/alan-turing.png",
    u"/_projects/progvis-sp/_imgs/computers/john-von-neumann_00.png",
    # Ivan Sutherland: SketchPad
    u"/_projects/progvis-sp/_imgs/computers/sketchpad_00.png",
    u"/_projects/progvis-sp/_imgs/computers/img0023.jpg",
    u"/_projects/progvis-sp/_imgs/computers/img0024.jpg",
    # Doug Engelbart: The Mother Of All Demos
    u"/_projects/progvis-sp/_imgs/computers/img0029.jpg",
    u"/_projects/progvis-sp/_imgs/computers/img0013.jpg",
    u"/_projects/progvis-sp/_imgs/computers/img0011.jpg",
    u"/_projects/progvis-sp/_imgs/computers/img0030.jpg",
    u"/_projects/progvis-sp/_imgs/computers/img0008.jpg",
    u"/_projects/progvis-sp/_imgs/computers/wke.png",
]

for img in images:
    newPage(800, 600)
    image(img, (0, 0))

saveImage('../computadores.pdf')
