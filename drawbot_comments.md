# DrawBot comments

Programação Visual workshop – São Paulo, May 2014

## 01

Wrong message for deprecated syntax of `image` command:

    >> image('http://patcomputerizedworld.files.wordpress.com/2014/03/guido-van-rossum_python.jpg', 0, 0)

    *** DrawBot warning: lowercase API is deprecated use: 'image("http://patcomputerizedworld.files.wordpress.com/2014/03/guido-van-rossum_python.jpg", (0, 0), alpha=None)' ***

There could be a dedicated message for this case, maybe something like this:

    Deprecated syntax, wrap x and y values in a tuple: …

## 02

The command `imageSize` returns values as floats.

    >> print imageSize('http://patcomputerizedworld.files.wordpress.com/2014/03/guido-van-rossum_python.jpg')

    (750.0, 819.0)

Shouldn’t they be integers instead?

## 03

Feature request: export syntax color settings and/or switch between syntax color stylesheets.

## 04

Colors selected with the Variables ColorWell widget throw an error when passed to gradients.

It would be nice if `gradient` could accept an `NSColor` like `fill` does.

    from AppKit import NSColor

    var_1 = {
        'name' : "c1",
        'ui' : "ColorWell",
        'args' : {
            'color' : NSColor.colorWithCalibratedRed_green_blue_alpha_(.3, .4, 1, .8),
        }
    }

    Variable([var_1], globals())

    size(300, 300)

    # passing the color to `fill` works
    fill(c1)
    oval(20, 20, 220, 220)

    linearGradient(
        (0, 0),
        (0, width()),
        # passing the color to gradient throws an error
        # [c1, (0,)],
        [(1, 0 ,0), (0,)],
        [0.0, 0.6],
    )

    oval(50, 50, 120, 120)

## 05

DrawBot’s implementation of float division by default makes things a bit confusing. It makes Python in DrawBot inconsistent with other Pythons (in Terminal and elsewhere). It goes against the ‘DrawBot is meant primarily for education, not production’ guideline -- it makes it harder to explain things.

There could be a page in the docs explaining things which are special about DrawBot’s Python, and why they are this way.

## 06

Hyphenation settings -- is it language-sensitive? how to get/set the current language?

