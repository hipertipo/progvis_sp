size(300, 300)

n = 10
m = 7
s = 20
x, y = 30, 30

for j in range(m):
    for i in range(n):
        c = random(),
        fill(*c)
        rect(x, y, s, s)
        y += s
    y -= s*n
    x += s
