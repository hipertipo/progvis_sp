# string formatting

participantes = [ 'Pedro', 'Maria', 'Gilberto', 'Ferdinando' ]
saudacao = ['Bom dia', 'Boa tarde', 'Boa noite']

for nome in participantes:
    # 'Bom dia Fulano, como vai você?'
    # print 'Bom dia ' + nome + ', como vai você?'
    print '%s %s, como vai você?' % (saudacao[0], nome)
