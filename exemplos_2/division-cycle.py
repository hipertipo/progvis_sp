
colors = [
    (1, 0, 0),
    (0, 1, 0),
    (0, 0, 1),
]

x = 0
w = 26

for i in range(25):
    # print i, i % 3
    color = colors[i % 3]
    fill(*color)
    rect(x, 0, w, height())
    x += w
