# em busca da palavra oculta

T = 'DrawBot é um ambiente para programação gráfica, desenvolvido por Just van Rossum para ensinar programação a alunos de design na KABK. O programa permite usar código em Python para gerar resultados visuais. Com isso podemos aplicar todo o poder da programação ao design gráfico: coisas difíceis ou impossíveis de serem executadas à mão podem ser resolvidas com algumas linhas de código. Isso traz maior eficência, e também abre portas para muitas possibilidades estimulantes.'

words = T.split()

secret_word = ''
for word in words:
    secret_word = secret_word + word[0]
    # secret_word += word[0]

print secret_word

L = list(secret_word)
L.reverse()
print ''.join(L)
