
L = ['a', 10, None, [], 'abcdef']

# itens da lista
for item in L:
    print item

# itens + contador
contador = 0
for item in L:
    print contador, item
    contador += 1

# indices + itens
for i in range(len(L)):
    print i, L[i]

# jeito elegante
for i, item in enumerate(L):
    print i, item
