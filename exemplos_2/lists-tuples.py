# lists vs. tuples

L = [1, 0, 0]
T = (1, 0, 0)

for item in L:
    print item

for item in T:
    print item

# single-element tuples

t = (1)
print type(t) # int!

t = (1,) # <-- comma needed
print type(t)

L[2] = 1
print L

# T[2] = 1
# print T

# un/packing tuples

r, g, b = T
b = 1
T = (r, g, b)
print T
