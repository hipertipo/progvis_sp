size(200, 200)

save()
translate(width()/3., height()/3.)

# criar o bezier
B = BezierPath()
B.moveTo((0, 0))
B.lineTo((112, -46))
B.lineTo((100, 100))
B.closePath()

# salva antes de entrar no clip
save()

# entra no clip
clipPath(B)
fill(0, 1, 0)
stroke(None)
rect(2, -30, 108, 88)
fill(1, .5, 0)
oval(32, -48, 52, 106)

# sai do clip
restore()

# desenha o fio vermelho (fora do clip)
fill(None)
stroke(1, 0, 0)
strokeWidth(10)
drawPath(B)
restore()

fill(0, 0, 1)
stroke(None)
rect(0, 0, 56, 76)
