# save and restore

size(500, 500)
translate(72, -56)

# vermelho
fill(1, 0, 0)
rect(242, 308, 100, 100)
save()

# verde
fill(0, 1, 0)
rect(218, 162, 100, 100)
save()

# azul
fill(0, 0, 1)
rotate(18)
rect(140, 152, 200, 200)

# volta pro verde
restore()
rect(48, 376, 122, 92)

# volta pro vermelho
restore()
rect(46, 144, 100, 100)
