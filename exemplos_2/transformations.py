size(100, 100)

rect(0, 0, 20, 20)

save()

translate(38, 44)
skew(18)
scale(1.72, 1.44)

fill(0, 0, 1)
rect(0, 0, 20, 32)

rotate(44)

fill(1, 0, 0, .5)
rect(0, 0, 20, 20)

restore()

rect(40, 10, 20, 20)
