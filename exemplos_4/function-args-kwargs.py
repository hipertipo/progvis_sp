# *args **kwargs

def my_func(a, *args, **kwargs):
    print a
    print args
    print kwargs['hello']
    
my_func(10, 'hello', 'hallo', hello='Maria')
