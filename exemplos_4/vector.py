# vector: a straight line with starting point, length and angle

def vector(p, d, a):
    x1, y1 = p
    angle_rad = radians(90-a)
    b = cos(angle_rad)
    c = sin(angle_rad)
    x2 = x1 + (d*c)
    y2 = y1 + (d*b)
    line((x1, y1), (x2, y2))

size(302, 344)

d = 198
a = 82

strokeWidth(13)
stroke(0, 0, 1)
lineCap('round')

vector((122, 72), d, a)
