# exemplo de objeto

class Rect:

    width = 100
    height = 100

    fill_color = (1, 0, 0)
    stroke_color = (0, 1, 0)
    stroke_width = 3

    def __init__(self):
        pass

    def draw(self, (x, y)):
        save()
        fill(*self.fill_color)
        stroke(*self.stroke_color)
        strokeWidth(self.stroke_width)
        rect(x, y, self.width, self.height)
        restore()

size(558, 578)

R = Rect()
R.fill_color = (1, 0, 1)
R.stroke_color = (0, 1, 1)
R.width, R.height = 312, 320
R.stroke_width = 39
R.draw((144, 152))

# default color settings
oval(58, 42, 220, 220)
