from drawBot import *

class Rect:

    width = 100
    height = 100

    fill_color = (1, 0, 0)
    stroke_color = (0, 1, 0)
    stroke_width = 3

    def __init__(self):
        pass

    def draw(self, (x, y)):
        save()
        fill(*self.fill_color)
        stroke(*self.stroke_color)
        strokeWidth(self.stroke_width)
        rect(x, y, self.width, self.height)
        restore()
