def modulo(x, y, w, h):
    translate(x, y)
    fill(randint(0, 1))
    # rotate(90 * randint(0, 1))
    polygon(
        (0, 0),
        (0, h),
        (w, h),
    )
    fill(randint(0, 1))
    polygon(
        (0, 0),
        (w, 0),
        (w, h),
    )

for i in range(20):
    newPage(200, 200)
    modulo(0, 0, width(), height())
    