# getRed:green:blue:alpha:
    
x = width() / 2
y = height() / 2
r = 390
rw = r * .7
rh = r * .5
r2 = r * .4

c1 = (0,)
c2 = (1,) 

oval(x-r, y-r, r*2, r*2)

fill(*c2)
rect(x-rw, y-rh, rw*2, rh*2)

fill(*c1)
oval(x-r2, y-r2, r2*2, r2*2)
