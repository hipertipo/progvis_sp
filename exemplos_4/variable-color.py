from AppKit import NSColor

var_1 = {
    'name' : "c1",
    'ui' : "ColorWell",
    'args' : {
        'color' : NSColor.colorWithCalibratedRed_green_blue_alpha_(.3, .4, 1, .8),
    }
}

Variable([var_1], globals())

size(300, 300)

# passing the color to `fill` works
fill(c1)
oval(20, 20, 220, 220)

linearGradient(
    (0, 0),
    (0, width()),
    # passing the color to gradient throws an error
    # [c1, (0,)],
    [(1, 0 ,0), (0,)],
    [0.0, 0.6],
)

oval(50, 50, 120, 120)
