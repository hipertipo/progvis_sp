from colorsys import hsv_to_rgb

h, s, v = 0.81, 1, 1
r, g, b = hsv_to_rgb(h, s, v)

fill(r, g, b)
rect(0, 0, width(), height())
