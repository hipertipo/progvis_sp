# cores hsb

from colorsys import hsv_to_rgb

n = 38
w = 10

s = 1
v = 1

color_step = 1.0 / (n-1)

size(392, 566)

for i in range(n):
    h = i * color_step
    r, g, b = hsv_to_rgb(h, s, v)
    fill(r, g, b)
    rect(0, 0, w, height())
    translate(w-1, 0)
