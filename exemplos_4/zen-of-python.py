import this
# reload(this)

# conteúdo do módulo `this`
# print dir(this)

# o que são essas coisas?
# print this.c # ?
# print this.d # isso parece a chave de uma cifra de substituição
# print this.s # isso parece ser texto que buscamos, porém cifrado
# print this.i # ?

# usando a chave da cifra pra decifrar o texto
zen = ''
for char in this.s:
    if this.d.has_key(char):
        zen += this.d[char]
    else:
        zen += char

# voilá
print zen

# exibindo o texto na página
size(210, 297)
lineHeight(7)
fontSize(4)
textBox(zen, (20, 20, width()-40, height()-40))
