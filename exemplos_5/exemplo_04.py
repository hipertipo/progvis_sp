x = 20
B = BezierPath()
for i in range(10):
    B.oval(x, 68, 20, 60)
    x += 25

save()
clipPath(B)
scale(.4)
image(u"/_projects/progvis-sp/_imgs/computers/img0029.jpg", (0, 0))
restore()

# print B.onCurvePoints
# print B.offCurvePoints

x, y, w, h = B.bounds()

fill(None)
stroke(0, 1, 0)
rect(x, y, w, h)
