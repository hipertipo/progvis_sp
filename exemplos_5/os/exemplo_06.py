fill(1, 0, 0)
oval(0, 0, 200, 200)

stroke(0, 0, 1)
fill(0, 1, 0)
B = BezierPath()
# contorno 1
B.moveTo((20, 20))
B.lineTo((20, 120))
B.lineTo((120, 120))
B.lineTo((120, 20))
B.closePath()
# contorno 2
B.moveTo((80, 80))
B.lineTo((100, 80))
B.lineTo((100, 100))
B.lineTo((80, 100))
B.closePath()

clipPath(B)

fill(1, 1, 0)
rect(0, 0, 300, 308)

print len(B.contours)

for c in B.contours:
    print c


