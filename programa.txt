==============================================================
programa do curso programação visual : 19-23 maio 2014
==============================================================

······························································
································#·····························
··························#·····#·····························
········####····#···#···#####···####·····###····####··········
········#···#···#···#·····#·····#···#···#···#···#···#·········
········#···#···#···#·····#·····#···#···#···#···#···#·········
········#···#····#·#······#·····#···#···#···#···#···#·········
········####······#········##···#···#····###····#···#·········
········#·········#···········································
········#·······##············································
······························································

--------------------------------------------------------------
participantes
--------------------------------------------------------------
Bruno Medeiros         bruno_medeiros@outlook.com      ###·#  
Diego Maldonado        diegommaldo@gmail.com           #####  
Diego Ribeiro          dgslvrbr@gmail.com              ####·  
Diogo Damasio          digo.sim@uol.com.br             ###-#  
Tony de Marco          tony@garoa.tv                   #####  
Manuel Scrofft         manuel.scrofft@gmail.com        #####  
Mateus Valadares       mateuscv@uol.com.br             #####  

==============================================================
dia 1 / 19 de maio / 19–23h
==============================================================

--------------------------------------------------------------
apresentações gerais, introdução ao workshop
--------------------------------------------------------------

introdução:

- apresentações individuais
- a história desse workshop (e a relação com o pyftgd)

características desse workshop:

- visão panorâmica da linguagem, conceitos
- estudo em grupo de diversos exemplos, técnicas etc.
- exercícios e exemplos para estudo e prática individual

programação para a semana:

2a : conceitos / Python (linguagem) <-- você está aqui!
3a : Python (exemplos) / DrawBot (linguagem)
4a : DrawBot (exemplos) / Python (objetos)
5a : Python (módulos) / discussão / Git
6a : outras ferramentas / exemplos / discussão

--------------------------------------------------------------
sobre computadores
--------------------------------------------------------------

- são mais antigos do que costumamos imaginar
- máquinas de calcular grandes e rápidas

## algumas imagens

- na pasta `_imgs`

## alguns links

Computers timeline
  http://www.computerhistory.org/revolution/timeline

Computer Pioneers
  http://youtube.com/watch?v=qundvme1Tik

Pioneer Computers
  http://youtube.com/watch?v=wsirYCAocZk

The Machine that Changed the World
  http://www.youtube.com/watch?v=rcR74y61xZk

Triumph of the Nerds: The Rise of Accidental Empires
  http://www.youtube.com/watch?v=3jV3JdtaOGc

Nerds 2.0.1: A Brief History of the Internet
  http://video.google.com/videoplay?docid=-643326150513935475#

All Watched Over By Machines Of Loving Grace
  http://vimeo.com/27393748

--------------------------------------------------------------
sobre linguagens
--------------------------------------------------------------

- linguagens naturais (humanos) vs. artificiais (máquinas)

seres humanos e linguagens naturais:

- sabemos lidar com ambiguidade (lemos nas entrelinhas)
- compreendemos contexto de um determinado enunciado
- temos uma história pessoal e memória viva

computadores e linguages artificiais:

- precisam de tudo explicado muito precisamente
- não compreendem contexto, não possuem livre arbítrio

sintaxe e estrutura de linguagens naturais:

- verbos, substantivos, adjetivos, advérbios, preposições…
- construção da frase, tempo do verbo, perguntas, etc…

sintaxe e estrutura de linguages de programação:

- números, sequências de caracteres, listas, dicionários…
- funções, objetos, módulos, bibliotecas…
- loops, construções de lógica, condicionais, comparações…
- exceções e erros

--------------------------------------------------------------
introdução à linguagem de programação Python
--------------------------------------------------------------

http://docs.pyftgd.org/python/introduction.html
http://docs.pyftgd.org/python/language.html

==============================================================
dia 2 / 20 de maio / 19–23h
==============================================================

O Zen do Python:

  import this

sabedoria de programadores:

- DRY: Don’t Repeat Yourself
- KISS: Keep It Simple Stupid
- Worse is Better
- Do the simplest thing that could possibly work.
- If it ain’t broke, don’t fix it.

--------------------------------------------------------------
exercícios
--------------------------------------------------------------

1. ‘em busca da mensagem oculta’

- pegue um trecho longo de um texto qualquer
- forme uma nova palavra a partir da primeira letra de cada
  palavra deste texto

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- divisão modulo
- divisão por inteiros
- divisão por zero (erro!)

- boolean logic: and, or

- nomes de variáveis
  http://docs.pyftgd.org/python/naming.html

- formatação de strings

- dicionários.get()
- sorted()
- dir()
- __doc__

- loops e contadores

--------------------------------------------------------------
DrawBot (continuação)
--------------------------------------------------------------

- loop dentro de loop (grid)

- font
- text
- lineHeight
- fontSize
- textBox

- gradientes
- sombra
- transformações
- recursão

- clipPath() / drawPath(B)
- converter texto para curva

- páginas
- interfaces

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- tuples

==============================================================
dia 3 / 21 de maio / 19–23h
==============================================================

Lynx, o mais antigo browser ainda em uso e desenvolvimento
  https://en.wikipedia.org/wiki/Lynx_%28web_browser%29

Markdown
  http://daringfireball.net/projects/markdown/

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- Python 2.x vs. Python 3

- If Guido was hit by a bus?
  http://legacy.python.org/search/hypermail/python-1994q2/1040.html

- Python Software Foundation
  http://python.org/

--------------------------------------------------------------
breve introdução ao Git
--------------------------------------------------------------

Get Started with Git
  http://alistapart.com/article/get-started-with-git

Alguns outros links
  https://pinboard.in/u:hipertipo/t:Git/

GitHub
  http://github.com/

BitBucket
  http://bitbucket.org/

--------------------------------------------------------------
DrawBot (continuação)
--------------------------------------------------------------

- textSize

- image / imageSize
- Variables
- hyphenation
- FormattedString

- lineDash
- BezierPath methods

- progressão de cor
- tabela de cores

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- funções

--------------------------------------------------------------
exercício
--------------------------------------------------------------

2. ‘agenda de contatos’

- criar um dicionário com pelo menos 4 pessoas
- para cada pessoa, definir: nome (chave), cidade, idade
- imprimir:
a. lista de nomes na ordem alfabética
b. lista de cidades na ordem alfabética
c. nome das pessoas: mais jovem e mais velha
d. quantos vivem em São Paulo
e. [BONUS] usar 'string formatting' para apresentar os dados

--------------------------------------------------------------
links
--------------------------------------------------------------

The LettError book
  http://issuu.com/letterror/docs/nypels_letterror_2000

==============================================================
dia 4 / 22 de maio / 19–23h
==============================================================

- configuração do projeto no BitBucket
- O Zen do Python --> no DrawBot

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- funções (continuação): *args & **kwargs

--------------------------------------------------------------
apresentação: programação visual
--------------------------------------------------------------

Bauhaus / Modernismo / Concretismo

- Joseph Albers
  https://duckduckgo.com/?q=!gi+josef+albers

- Anni Albers
  https://duckduckgo.com/?q=!w+anni+albers
  https://duckduckgo.com/?q=!gi+anni+albers

- Hércules Barsotti
  https://duckduckgo.com/?q=!gi+hercules+barsotti

- Max Bill
  https://duckduckgo.com/?q=!gi+max+bill

- Luis Sacilotto
  https://duckduckgo.com/?q=!gi+luis+sacilotto

- Richard Paul Lohse
  https://duckduckgo.com/?q=!gi+richard+paul+lohse

Computadores analógicos

- Herbert Franke
  https://duckduckgo.com/?q=!gi+herbert+franke

- James Whitney
  https://duckduckgo.com/?q=!yt+james+whitney

- Spirograph
  https://duckduckgo.com/?q=!w+spirograph
  https://duckduckgo.com/?q=!gi+spirograph

Imagem digital (pixel)

- Ken Knowlton
  https://duckduckgo.com/?q=!gi+ken+knowlton

Design programs

- Karl Gerstner
  https://duckduckgo.com/?q=!gi+karl+gerstner

- Anton Stankowski
  https://duckduckgo.com/?q=!gi+anton+stankowski

- Aloísio Magalhães
  https://duckduckgo.com/?q=!gi+aloisio+magalhaes

- Otl Aicher
  https://duckduckgo.com/?q=!gi+otl+aicher

- Jurriaan Schrofer
  https://duckduckgo.com/?q=!gi+jurriaan+schrofer

Design programado

- Michael Noll
  https://duckduckgo.com/?q=!gi+michael+noll

- Frieder Nake
  https://duckduckgo.com/?q=!gi+frieder+nake

- Manfred Mohr
  https://duckduckgo.com/?q=!gi+manfred+mohr

- Sol Lewitt
  https://duckduckgo.com/?q=!gi+sol+lewitt

Visualização de informações

- Edward Tufte: Envisioning Information
  https://duckduckgo.com/?q=!gi+edward+tufte+envisioning+information

- Fat Fonts
  http://fatfonts.org/
  http://innovis.cpsc.ucalgary.ca/innovis/uploads/Publications/Publications/FatFonts.pdf
  http://visual.ly/fatfonts-player?view=true

--------------------------------------------------------------
exercícios
--------------------------------------------------------------

recriando gráficos programáticos:

- Anni Albers
- marca da Rede Globo

Seymour Papert / Logo / Turtle graphics

- https://duckduckgo.com/?q=!w+turtle+graphics
- https://duckduckgo.com/?q=!gi+turtle+graphics

L-Systems

- https://en.wikipedia.org/wiki/L-system

--------------------------------------------------------------
exercício: paginação automática
--------------------------------------------------------------

- texto fluindo por várias páginas
- páginas pares / ímpares
- layout espelhado: margens, numeração

--------------------------------------------------------------
Python (continuação)
--------------------------------------------------------------

- módulos
- objetos

--------------------------------------------------------------
DrawBot (continuação)
--------------------------------------------------------------

- usando cores hsb
- relógio analógico / digital

==============================================================
dia 5 / 23 de maio / 19–23h
==============================================================

- editores de Markdown: Mou, LightPaper

** humor de programador **

Yoda Conditions etc.
  http://www.dodgycoder.net/2011/11/yoda-conditions-pokemon-exception.html

WAT
  https://www.destroyallsoftware.com/talks/wat

** inspiração **

Zen e a Arte da Manutenção de Motocicletas -- Robert Pirsig

--------------------------------------------------------------
linguagens de programação 
--------------------------------------------------------------

- C
- Lisp
- SmallTalk
- Java
- JavaScript
- PHP
- Perl
- Ruby
- Objective-C

Brainfuck
  https://en.wikipedia.org/wiki/Brainfuck#Hello_World.21

Piet
  http://www.dangermouse.net/esoteric/piet.html
  http://www.dangermouse.net/esoteric/piet/samples.html

--------------------------------------------------------------
DrawBot (continuação)
--------------------------------------------------------------

- BezierPath: formas vazadas
- BezierPath.contours

- BezierPath.rect()
- BezierPath.oval()
- BezierPath.bounds()

--------------------------------------------------------------
Python (continuação): módulos
--------------------------------------------------------------

- os
- colorsys
- time
- csv
- plistlib
- PIL

** instalando módulos **

- markdown
- colorsys
- progvis

--------------------------------------------------------------
provis: ferramentas para DrawBot/NodeBox
--------------------------------------------------------------

- progvis.objects
- progvis.modules
- progvis.fonts

--------------------------------------------------------------
DrawBot (continuação): UFOs & RoboFab
--------------------------------------------------------------

- o formato UFO
- a biblioteca de código RoboFab
- ufoText (progvis)

--------------------------------------------------------------
Digital Roots: Arquelogia digital
--------------------------------------------------------------

Computer Graphics and Art
  http://toplap.org/pdfs-of-computer-graphics-and-art/

ReCode Project
  http://recodeproject.com/

--------------------------------------------------------------
mantendo a chama acesa
--------------------------------------------------------------

** software livre vs. de código aberto **

FSI (Free Software Foundation)
OSI (Open Source Initiative)

Richard Stallman
  https://duckduckgo.com/?q=!w+richard_stallman
  https://www.gnu.org/
  http://www.jwz.org/hacks/why-cooperation-with-rms-is-impossible.mp3
  https://www.gnu.org/music/free-software-song.html

The Cathedral and the Bazaar
  https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar
  http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/index.html

** Licenças de uso software **

- BSD
- MIT
- GNU-GPL / GPL3
- WTFPL

** manutenção e documentação de código **

Literate Programming
  https://en.wikipedia.org/wiki/Literate_programming

Pycco (Docco for Python)
  https://fitzgen.github.io/pycco/

** ferramentas de documentação do Python **

- doctest
- docutils
- ReST
- Git: clone, fork, pull request
- GitHub / BitBucket: issues, wiki, comments
- Sphinx
- ReadTheDocs

** convivendo em harmonia **

- ‘Não mije na piscina!’

- Ética hacker
  https://en.wikipedia.org/wiki/Hacker_ethics

- Netiquette
  http://tools.ietf.org/html/rfc1855
  http://www.bbc.co.uk/webwise/guides/about-netiquette
  http://learning.colostate.edu/guides/guide.cfm?guideid=4
  …etc…

--------------------------------------------------------------
mensagem final
--------------------------------------------------------------

vemos mais longe porque estamos sobre os ombros de gigantes

- sempre dar crédito para quem merece!
- sempre citar fontes e referências!

Sharing Interviews
  https://github.com/kylemcdonald/SharingInterviews

passado > presente > futuro

- recuperando o atraso: estudar o que já foi feito
- ir além: acrescentar algo de novo

==============================================================
links
==============================================================

--------------------------------------------------------------
Computadores
--------------------------------------------------------------

Computer History Museum
  http://www.computerhistory.org/exhibits/

DigiBarn
  http://www.digibarn.com/

--------------------------------------------------------------
Douglas Engelbart
--------------------------------------------------------------

The Mother of All Demos
  https://www.youtube.com/watch?v=yJDv-zdhzMY

Augmenting Human Intellect
  http://www.dougengelbart.org/pubs/augment-3906.html

--------------------------------------------------------------
Joseph Weizenbaum
--------------------------------------------------------------

Plug & Pray
  http://www.plugandpray-film.de/en/

Rebel At Work
  http://www.ilmarefilm.org/W_E_1.htm

--------------------------------------------------------------
randomness
--------------------------------------------------------------

https://en.wikipedia.org/wiki/Randomness
https://en.wikipedia.org/wiki/Hardware_random_number_generator
https://en.wikipedia.org/wiki/Random_number_generation

--------------------------------------------------------------
Metafont etc.
--------------------------------------------------------------

Metafont
  http://en.wikipedia.org/wiki/Metafont

Metaflop
  http://metaflop.com/

Metapolator
  http://metapolator.com/

--------------------------------------------------------------
outras ferramentas de programção visual
--------------------------------------------------------------

Processing
  http://processing.org/
  https://github.com/jdf/processing.py

Scriptographer
  http://scriptographer.org/

Paper.js
  http://paperjs.org/

Two.js
  https://jonobr1.github.io/two.js/

Three.js
  http://threejs.org/

Basil.js
  http://basiljs.ch/

==============================================================
# EOF
--------------------------------------------------------------
documentação   >>> https://bitbucket.org/hipertipo/progvis_sp
links diversos >>> https://pinboard.in/u:hipertipo
--------------------------------------------------------------
