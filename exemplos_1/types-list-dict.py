
a = 'boa noite, '
b = 10
c = 'fulano'

print a * b
print a + c

L = [ 'a', 10, 200, [ 'a', 'askd' ], 789, 30.3, 'asd' ]
M = L + ['bbbbbb', 'ccc']

print L[0]
print L[-1]
print L[2:4]

D = {
    'fred' : 400,
    'tony' : 205,
    'diogo' : 32,
    'diego' : [ 'a', 'b' ] 
}

print D.has_key('fred')
D['rodrigo'] = 330

print D.keys()
