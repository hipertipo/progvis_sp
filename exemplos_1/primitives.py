size(518, 464)

# background
fill(.5)
rect(0, 0, width(), height())

fill(1, 1, 0)
oval(236, 142, 244, 266)

lineCap('square')
lineJoin('round')

stroke(0, 0, 1)
strokeWidth(18)

line((48, 54), (486, 412))

fill(0, 1, 0, .5)
polygon((114, 62), (256, 72), (160, 346), (38, 244))
