size(618, 412)

n = 24

B = BezierPath()
for i in range(n):
    fill(random(), random(), random())
    x = randint(0, width())
    y = randint(0, height())
    s = randint(10, 30)
    if i == 0:
        # print 'primeiro ponto!'
        B.moveTo((x, y))
    else:
        # print 'outro ponto'
        B.lineTo((x, y))

fill(None)
stroke(1, 0, 0)
strokeWidth(3)
drawPath(B)
