# bezier

size(324, 368)
stroke(1, 0, 0)
strokeWidth(5)
fill(None)

# newPath()
# moveTo((48, 20))
# lineTo((58, 140))
# curveTo((-48, 176), (162, 218), (156, 78))
# drawPath()

B = BezierPath()
B.moveTo((50, 84))
B.lineTo((108, 76))
B.curveTo((52, 234), (204, 292), (258, 124))
drawPath(B)

d = 22

for p in B.points:
    x, y = p
    print p, x, y
    oval(x-(d/2.), y-(d/2.), d, d)
