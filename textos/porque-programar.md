# Porque programar?

- para executar tarefas repetitivas rapidamente
- para explorar inúmeras variações de uma idéia
- para melhorar nossa qualidade de vida

A vida é muito curta para se perder tempo executando tarefas repetitivas: para isso, existem computadores.

Muito do trabalho do designer gráfico (ou *programador visual*, como também chamamos a profissão em português) envolve a sistematização do uso de elementos visuais: cores, formas, posicionamento, dimensões etc. Definimos regras visuais em seus mínimos detalhes, para depois implementarmos tudo *manualmente* em manuais de identidade visual, layouts e artes-finais. Isso não faz muito sentido.

**Se conseguirmos expressar essas mesmas regras em código, a sua execução pode ser toda relegada às máquinas. Elas fazem isso muito mais rápido e com muito mais precisão que nós.**

Mais do que ajudar apenas na *execução* de projetos, computadores também podem ser úteis na *criação e geração de idéias*. Usando variáveis e parâmetros aleatórios, podemos fazer com que a máquina produza inúmeras opções diferentes de uma composição gráfica – cabendo a nós apenas escolher aquela(s) de que gostamos mais. Ou, se nenhuma das opções nos agradar, ajustamos os algoritmos e tentamos de novo.

Computadores não tornarão o ser humano obsoleto (pelo menos não tão cedo). Eles não podem ver nem compreender. Não possuem senso estético, e não sabem lidar com ambiguidade – precisam que expliquemos tudo nos mínimos detalhes. Eles são na verdade incrivelmente burros. Seu valor está no benefício que eles podem trazer para a vida dos seres humanos quando bem utilizados.
