# Porque Python?

Python é uma linguagem de programação moderna com ênfase em clareza, simplicidade e legibilidade – o que faz com que ela seja uma excelente primeira linguagem de programação. A filosofia por trás da linguagem é resumida no “Zen do Python”, uma coleção de aforismos que inclui:

- Bonito é melhor do que feio
- Explícito é melhor que implícito
- Simples é melhor que complexo
- Complexo é melhor que complicado
- Legibilidade conta

Python é uma linguagem de programação open-source, disponível gratuitamente nas principais plataformas. É usada por grandes e pequenas organizações nas mais diferentes áreas: para desenvolvimento de websites, para cálculos matemáticos e científicos, para processamento de linguagem natural etc.

Python possui uma relação especial com o mundo do design. Seu criador, o cientista da computação holandês Guido van Rossum, é irmão mais velho de Just van Rossum, metade da dupla de designers-programadores LettError (com Erik van Blokland). Juntos ou separados, eles são conhecidos desde os anos 1990 por seu trabalho inovador em design de tipos e desenvolvimento de ferramentas para designers – tudo feito com Python.

E para aqueles que se perguntam de onde veio o nome Python – sim, ele é inspirado no grupo de comediantes Monty Python. Programação não precisa ser algo sério!
